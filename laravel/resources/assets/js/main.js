import AjaxSetup from "./AjaxSetup";
// import MobileToggle from "./MobileToggle";

AjaxSetup();
// MobileToggle();

$(document).ready(function () {
  // BANNERS + PAGERS
  $(".banners").cycle({
    slides: ".banner",
    fx: "fade",
    pager: ".cycle-pager",
  });

  var quantBanners = $(".banner").length;
  var widthPagers = 100 / quantBanners + "%";
  $(".cycle-pager > span").css("width", widthPagers);
});
