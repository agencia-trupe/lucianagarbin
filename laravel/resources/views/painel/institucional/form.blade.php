@include('painel.common.flash')

<div class="well form-group">
    {!! Form::label('imagem', 'Imagem') !!}
    @if($registro->imagem)
    <img src="{{ url('assets/img/institucional/'.$registro->imagem) }}" style="display:block; margin-bottom: 10px; max-width: 100%;">
    @endif
    {!! Form::file('imagem', ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('registro', 'Registro') !!}
    {!! Form::text('registro', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_formacao', 'Texto Formação') !!}
    {!! Form::textarea('texto_formacao', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('texto_perfil', 'Texto Perfil') !!}
    {!! Form::textarea('texto_perfil', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

<div class="form-group">
    {!! Form::label('frase_banner', 'Frase Banner') !!}
    {!! Form::text('frase_banner', null, ['class' => 'form-control']) !!}
</div>


{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}