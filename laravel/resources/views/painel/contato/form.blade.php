@include('painel.common.flash')

<div class="form-group">
    {!! Form::label('nome', 'Nome') !!}
    {!! Form::text('nome', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('email', 'E-mail') !!}
    {!! Form::text('email', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('telefone', 'Telefone') !!}
    {!! Form::text('telefone', null, ['class' => 'form-control']) !!}
</div>

<hr>
<h3>Localização 1</h3>

<div class="form-group">
    {!! Form::label('local_1', 'Nome do Local') !!}
    {!! Form::text('local_1', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('endereco_1', 'Endereço') !!}
    {!! Form::text('endereco_1', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cidade_1', 'Cidade-UF') !!}
    {!! Form::text('cidade_1', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('google_maps_1', 'Código GoogleMaps') !!}
    {!! Form::text('google_maps_1', null, ['class' => 'form-control']) !!}
</div>

<hr>
<h3>Localização 2</h3>

<div class="form-group">
    {!! Form::label('local_2', 'Nome do Local') !!}
    {!! Form::text('local_2', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('endereco_2', 'Endereço') !!}
    {!! Form::text('endereco_2', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('cidade_2', 'Cidade-UF') !!}
    {!! Form::text('cidade_2', null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
    {!! Form::label('google_maps_2', 'Código GoogleMaps') !!}
    {!! Form::text('google_maps_2', null, ['class' => 'form-control']) !!}
</div>

<hr>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
