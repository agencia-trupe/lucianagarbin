@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Áreas de Atuação /</small> Editar Área</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.areas-atuacao.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.areas-atuacao.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
