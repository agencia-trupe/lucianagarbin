@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Áreas de Atuação /</small> Adicionar Área</h2>
    </legend>

    {!! Form::open(['route' => 'painel.areas-atuacao.store', 'files' => true]) !!}

        @include('painel.areas-atuacao.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
