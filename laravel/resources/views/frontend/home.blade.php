@extends('frontend.common.template')

@section('content')

<div class="home">
    <div class="banners">
        <div class=cycle-pager></div>
        @foreach($banners as $banner)
        <div class="banner" style="background-image: url({{ asset('assets/img/banners/'.$banner->imagem) }})"></div>
        @endforeach
        <p class="frase-banner">{{ $institucional->frase_banner }}</p>
    </div>
    <div class="institucional">
        <div class="dados-pessoa">
            <img src="{{ asset('assets/img/institucional/'.$institucional->imagem) }}" alt="">
            <p class="nome">{{ $institucional->nome }}</p>
            <p class="registro">{{ $institucional->registro }}</p>
        </div>
        <div class="textos-perfil">
            <div class="formacao">{!! $institucional->texto_formacao !!}</div>
            <div class="perfil">{!! $institucional->texto_perfil !!}</div>
        </div>
    </div>
    <div class="areas-atuacao">
        <h2 class="titulo">Áreas de Atuação:</h2>
        @foreach($areasAtuacao as $area)
        <div class="area">
            <p class="area-titulo">{{ $area->titulo }}</p>
            <div class="area-texto">{!! $area->texto !!}</div>
        </div>
        @endforeach
    </div>
    <div class="contatos">
        <h2 class="titulo">Contato</h2>
        <a href="https://api.whatsapp.com/send?phone={{ $whatsapp }}" class="telefone" target="_blank">{{ $contato->telefone }}</a>

        <form action="{{ route('contato.post') }}" method="POST">
            {!! csrf_field() !!}
            <div class="form-contato">
                <div class="dados">
                    <input type="text" name="nome" placeholder="nome" value="{{ old('nome') }}" required>
                    <input type="email" name="email" placeholder="e-mail" value="{{ old('email') }}" required>
                    <input type="text" name="telefone" placeholder="telefone" value="{{ old('telefone') }}">
                </div>
                <textarea name="mensagem" placeholder="mensagem" required>{{ old('mensagem') }}</textarea>
                <button type="submit" class="btn-contato">Enviar</button>
            </div>

            @if($errors->any())
            <div class="flash flash-erro">
                @foreach($errors->all() as $error)
                {!! $error !!}<br>
                @endforeach
            </div>
            @endif

            @if(session('enviado'))
            <div class="flash flash-sucesso">
                Mensagem enviada com sucesso!
            </div>
            @endif
        </form>

        <div class="enderecos">
            <div class="endereco1">
                <p class="local1">{{ $contato->local_1 }}</p>
                <p class="end1">{{ $contato->endereco_1 }}</p>
                <p class="cidade1">{{ $contato->cidade_1 }}</p>
                <div class="mapa1">
                    {!! $contato->google_maps_1 !!}
                </div>
            </div>
            <div class="endereco2">
                <p class="local2">{{ $contato->local_2 }}</p>
                <p class="end2">{{ $contato->endereco_2 }}</p>
                <p class="cidade2">{{ $contato->cidade_2 }}</p>
                <div class="mapa2">
                    {!! $contato->google_maps_2 !!}
                </div>
            </div>
        </div>
    </div>
</div>

@endsection