<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AreaAtuacao extends Model
{
    protected $table = 'areas_atuacao';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }

}
