<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class InstitucionalRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'imagem'         => 'image',
            'nome'           => 'required',
            'registro'       => 'required',
            'texto_formacao' => 'required',
            'texto_perfil'   => 'required',
            'frase_banner'   => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente',
        ];
    }
}
