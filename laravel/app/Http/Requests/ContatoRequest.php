<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ContatoRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'nome'          => 'required',
            'email'         => 'required|email',
            'telefone'      => 'required',
            'local_1'       => 'required',
            'endereco_1'    => 'required',
            'cidade_1'      => 'required',
            'google_maps_1' => 'required',
            'local_2'       => 'required',
            'endereco_2'    => 'required',
            'cidade_2'      => 'required',
            'google_maps_2' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'required' => 'Preencha todos os campos corretamente',
            'email'    => 'Preencha todos os campos corretamente',
        ];
    }
}
