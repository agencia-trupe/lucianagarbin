<?php

namespace App\Http\Controllers\Painel;

use App\Http\Controllers\Controller;
use App\Http\Requests\AreasAtuacaoRequest;
use App\Models\AreaAtuacao;

class AreasAtuacaoController extends Controller
{
    public function index()
    {
        $registros = AreaAtuacao::ordenados()->get();

        return view('painel.areas-atuacao.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.areas-atuacao.create');
    }

    public function store(AreasAtuacaoRequest $request)
    {
        try {
            $input = $request->all();

            AreaAtuacao::create($input);

            return redirect()->route('painel.areas-atuacao.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);
        }
    }

    public function edit(AreaAtuacao $registro)
    {
        return view('painel.areas-atuacao.edit', compact('registro'));
    }

    public function update(AreasAtuacaoRequest $request, AreaAtuacao $registro)
    {
        try {
            $input = $request->all();

            $registro->update($input);

            return redirect()->route('painel.areas-atuacao.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }

    public function destroy(AreaAtuacao $registro)
    {
        try {
            $registro->delete();

            return redirect()->route('painel.areas-atuacao.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);
        }
    }

}
