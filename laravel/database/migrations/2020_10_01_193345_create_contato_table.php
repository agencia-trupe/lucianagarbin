<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\Schema;

class CreateContatoTable extends Migration
{
    public function up()
    {
        Schema::create('contato', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nome');
            $table->string('email');
            $table->string('telefone');
            $table->string('local_1');
            $table->string('endereco_1');
            $table->string('cidade_1');
            $table->text('google_maps_1');
            $table->string('local_2');
            $table->string('endereco_2');
            $table->string('cidade_2');
            $table->text('google_maps_2');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('contato');
    }
}
