<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class InstitucionalTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('institucional')->insert([
            'imagem'         => '',
            'nome'           => 'Luciana Maria Biscaia dos Santos Garbin',
            'registro'       => 'CRP 06/31796-9',
            'texto_formacao' => '',
            'texto_perfil'   => '',
            'frase_banner'   => 'Psicologia Clínica & Neuropsicologia',
        ]);
    }
}
