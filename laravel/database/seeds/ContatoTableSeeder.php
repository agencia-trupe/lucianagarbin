<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ContatoTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('contato')->insert([
            'nome'          => 'Luciana Biscaia Garbin',
            'email'         => 'contato@trupe.net',
            'telefone'      => '11 99268 7220',
            'local_1'       => 'Itaim Bibi',
            'endereco_1'    => 'Rua Prof. Atílio Innocenti 474 ∙ cj. 407',
            'cidade_1'      => 'São Paulo ∙ SP',
            'google_maps_1' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3074.5965026472163!2d-46.682219576104195!3d-23.592422036117302!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce57454a8716b9%3A0x299439f10c27b146!2sR.%20Prof.%20At%C3%ADlio%20Innocenti%2C%20474%20-%20Vila%20Nova%20Concei%C3%A7%C3%A3o%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2004538-001!5e0!3m2!1spt-BR!2sbr!4v1601581451987!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>',
            'local_2'       => 'Real Parque | Morumbi',
            'endereco_2'    => 'Avenida Barão de Campos Gerais 552',
            'cidade_2'      => 'São Paulo ∙ SP',
            'google_maps_2' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.8608840234433!2d-46.70937408538282!3d-23.609321669249425!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce572af972de23%3A0x8a0b32733cd211d!2sAv.%20Bar%C3%A3o%20de%20Campos%20Gerais%2C%20552%20-%20Real%20Parque%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2005684-001!5e0!3m2!1spt-BR!2sbr!4v1601581556426!5m2!1spt-BR!2sbr" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>',
        ]);
    }
}
