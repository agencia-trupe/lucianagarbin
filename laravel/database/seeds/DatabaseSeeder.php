<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(ContatoTableSeeder::class);
        $this->call(ConfiguracoesTableSeeder::class);
        $this->call(InstitucionalTableSeeder::class);
    }
}
