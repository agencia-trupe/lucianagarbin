--
-- Table structure for table `areas_atuacao`
--

DROP TABLE IF EXISTS `areas_atuacao`;
CREATE TABLE `areas_atuacao` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `titulo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `areas_atuacao`
--

LOCK TABLES `areas_atuacao` WRITE;
INSERT INTO `areas_atuacao` VALUES (1,1,'Psicologia Clínica','<p><span style=\"font-size:12pt\"><span style=\"font-family:Calibri,sans-serif\">O processo psicoter&aacute;pico visa a ajudar o indiv&iacute;duo a acessar seus recursos ps&iacute;quicos para lidar com dificuldades e desenvolver, assim, seus aspectos pessoal, profissional e acad&ecirc;mico.</span></span></p>\r\n\r\n<p><span style=\"font-size:12pt\"><span style=\"font-family:Calibri,sans-serif\">As sess&otilde;es s&atilde;o conduzidas por meio de t&eacute;cnicas espec&iacute;ficas a cada faixa et&aacute;ria, que possibilitam ao paciente melhor compreender as dificuldades que enfrenta - as quais podem incluir quest&otilde;es emocionais, de aprendizagem e de relacionamento interpessoal, dentre outras - possibilitando o desenvolvimento de recursos para super&aacute;-las.</span></span></p>\r\n','2020-10-06 21:45:08','2020-10-06 21:45:08'),(2,2,'Orientação de pais e familiares','<p><span style=\"font-size:12pt\"><span style=\"font-family:Calibri,sans-serif\">Objetiva auxiliar pais e familiares na compreens&atilde;o das particularidades de cada fase de desenvolvimento da crian&ccedil;a e adolescente. Assim, busca-se a promo&ccedil;&atilde;o de &nbsp;um melhor relacionamento familiar, fornecendo instrumentos para auxiliar a resolu&ccedil;&atilde;o de&nbsp; conflitos e ang&uacute;stias. </span></span></p>\r\n','2020-10-06 21:46:07','2020-10-06 21:46:07'),(3,3,'Avaliação Neuropsicológica','<p><span style=\"font-size:12pt\"><span style=\"font-family:Calibri,sans-serif\">Tem como objetivo levantar o perfil do funcionamento cognitivo do paciente, por meio de testes e instrumentos padronizados e validados pelo Conselho Federal de Psicologia. S&atilde;o investigados e avaliados os diversos dom&iacute;nios cognitivos (mem&oacute;ria, aten&ccedil;&atilde;o, fun&ccedil;&otilde;es executivas, linguagem, etc.) e poss&iacute;veis&nbsp; altera&ccedil;&otilde;es e d&eacute;ficits do Sistema Nervoso Central e do Comportamento, para auxiliar em diagn&oacute;sticos diferenciais, como Transtornos de Aprendizagem, do Espectro Autista, do D&eacute;ficit de Aten&ccedil;&atilde;o e Hiperatividade, do Humor, Neuropsiqui&aacute;tricos, Quadros Demenciais, dentre outros.</span></span></p>\r\n\r\n<p><span style=\"font-size:12pt\"><span style=\"font-family:Calibri,sans-serif\">Al&eacute;m de auxiliar no diagn&oacute;stico, a avalia&ccedil;&atilde;o permite compreender a natureza e o grau das altera&ccedil;&otilde;es neuropsicol&oacute;gicas, possibilitando a elabora&ccedil;&atilde;o de um programa de reabilita&ccedil;&atilde;o e acompanhamento adequado para cada caso.</span></span></p>\r\n','2020-10-06 21:46:35','2020-10-06 21:46:35'),(4,4,'Reabilitação Neuropsicológica','<p><span style=\"font-size:12pt\"><span style=\"font-family:Calibri,sans-serif\">Visa a auxiliar o paciente que apresente altera&ccedil;&otilde;es neuropsicol&oacute;gicas detectadas, em avalia&ccedil;&atilde;o, no restabelecimento e na adapta&ccedil;&atilde;o do indiv&iacute;duo, de modo a atenuar os impactos do seu quadro em atividades cotidianas, profissionais, acad&ecirc;micas e em rela&ccedil;&otilde;es interpessoais.</span></span></p>\r\n','2020-10-06 21:47:00','2020-10-06 21:47:00');
UNLOCK TABLES;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT 0,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
INSERT INTO `banners` VALUES (1,1,'banner1_20201006184244.png','2020-10-06 21:42:45','2020-10-06 21:42:45'),(2,2,'banner2_20201006184253.png','2020-10-06 21:42:54','2020-10-06 21:42:54'),(3,3,'banner3_20201006184303.png','2020-10-06 21:43:04','2020-10-06 21:43:04'),(4,4,'banner4_20201006184313.png','2020-10-06 21:43:13','2020-10-06 21:43:13');
UNLOCK TABLES;

--
-- Table structure for table `configuracoes`
--

DROP TABLE IF EXISTS `configuracoes`;
CREATE TABLE `configuracoes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `keywords` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `imagem_de_compartilhamento` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `analytics` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `configuracoes`
--

LOCK TABLES `configuracoes` WRITE;
INSERT INTO `configuracoes` VALUES (1,'Luciana Garbin','','','','',NULL,NULL);
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `local_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `endereco_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cidade_1` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `google_maps_1` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `local_2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `endereco_2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `cidade_2` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `google_maps_2` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
INSERT INTO `contato` VALUES (1,'Luciana Biscaia Garbin','contato@trupe.net','11 99268 7220','Itaim Bibi','Rua Prof. Atílio Innocenti 474 ∙ cj. 407','São Paulo ∙ SP','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3074.5965026472163!2d-46.682219576104195!3d-23.592422036117302!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce57454a8716b9%3A0x299439f10c27b146!2sR.%20Prof.%20At%C3%ADlio%20Innocenti%2C%20474%20-%20Vila%20Nova%20Concei%C3%A7%C3%A3o%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2004538-001!5e0!3m2!1spt-BR!2sbr!4v1601581451987!5m2!1spt-BR!2sbr\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>','Real Parque | Morumbi','Avenida Barão de Campos Gerais 552','São Paulo ∙ SP','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3655.8608840234433!2d-46.70937408538282!3d-23.609321669249425!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ce572af972de23%3A0x8a0b32733cd211d!2sAv.%20Bar%C3%A3o%20de%20Campos%20Gerais%2C%20552%20-%20Real%20Parque%2C%20S%C3%A3o%20Paulo%20-%20SP%2C%2005684-001!5e0!3m2!1spt-BR!2sbr!4v1601581556426!5m2!1spt-BR!2sbr\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0;\" allowfullscreen=\"\" aria-hidden=\"false\" tabindex=\"0\"></iframe>',NULL,NULL);
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
CREATE TABLE `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
UNLOCK TABLES;

--
-- Table structure for table `institucional`
--

DROP TABLE IF EXISTS `institucional`;
CREATE TABLE `institucional` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nome` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `registro` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto_formacao` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `texto_perfil` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `frase_banner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `institucional`
--

LOCK TABLES `institucional` WRITE;
INSERT INTO `institucional` VALUES (1,'foto-lucianagarbin_20201006184428.png','Luciana Maria Biscaia dos Santos Garbin','CRP 06/31796-9','<p><span style=\"font-size:12pt\"><span style=\"font-family:Calibri,sans-serif\">Psic&oacute;loga Cl&iacute;nica, Neuropsic&oacute;loga (Faculdade de Medicina da USP). Especializa&ccedil;&atilde;o em Reabilita&ccedil;&atilde;o Neuropsicol&oacute;gica (Faculdade de Medicina da USP). Forma&ccedil;&atilde;o em Dislexia (Associa&ccedil;&atilde;o Brasileira de Dislexia) e Terapia Cognitivo Comportamental (CETCC). </span></span></p>\r\n\r\n<p><span style=\"font-size:12pt\"><span style=\"font-family:Calibri,sans-serif\">Aprimoramento em Psicologia Hospitalar (Faculdade de Medicina da USP) e est&aacute;gios em diversas institui&ccedil;&otilde;es cl&iacute;nicas (AACD, Vancouver Children&rsquo;s Hospital).</span></span></p>\r\n','<p><span style=\"font-size:12pt\"><span style=\"font-family:Calibri,sans-serif\">&ldquo;Desde o in&iacute;cio, optei pelo atendimento cl&iacute;nico voltado a um p&uacute;blico amplo, abrangendo crian&ccedil;as, adolescentes, adultos e idosos. A neuropsicologia veio complementar a minha forma&ccedil;&atilde;o e pr&aacute;tica profissional. Desde ent&atilde;o, passei a realizar avalia&ccedil;&otilde;es e reabilita&ccedil;&otilde;es neuropsicol&oacute;gicas, bem como a orientar pais e familiares dos pacientes atendidos.</span></span></p>\r\n\r\n<p><span style=\"font-size:12pt\"><span style=\"font-family:Calibri,sans-serif\">Al&eacute;m disso, sempre me mantive envolvida, como volunt&aacute;ria, em institui&ccedil;&otilde;es de ensino, desenvolvendo pesquisas emp&iacute;ricas junto a pacientes portadores de Transtorno do D&eacute;ficit de Aten&ccedil;&atilde;o e Hiperatividade e Transtornos de Aprendizagem.</span></span></p>\r\n\r\n<p><span style=\"font-size:12.0pt\"><span style=\"font-family:&quot;Calibri&quot;,sans-serif\">Meu trabalho visa a auxiliar na compreens&atilde;o e no desenvolvimento do indiv&iacute;duo, nas diversas fases de sua vida.&rdquo;</span></span></p>\r\n','Psicologia Clínica & Neuropsicologia',NULL,'2020-10-06 21:44:28');
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
INSERT INTO `migrations` VALUES ('2014_10_12_000000_create_users_table',1),('2020_10_01_193345_create_contato_table',1),('2020_10_01_194946_create_contatos_recebidos_table',1),('2020_10_01_195042_create_banners_table',1),('2020_10_01_195226_create_configuracoes_table',1),('2020_10_01_195617_create_institucional_table',1),('2020_10_01_200700_create_areas_atuacao_table',1);
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net','$2y$10$vvxB4xrhsHbZIRqJUDmCj.uLm./BSxsyoXSTIbJeojuyUXJYop3Me','v6skU9ynIKgep7IbIXZr3ruJXby79fWDr143sWWkeWzKmC0aIJW4ybwNbP9o',NULL,'2020-10-06 21:41:08');
UNLOCK TABLES;

-- Dump completed on 2020-10-06 15:54:59
